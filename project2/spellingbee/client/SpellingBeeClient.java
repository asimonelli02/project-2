package spellingbee.client;
import java.io.IOException;

import javafx.application.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;

public class SpellingBeeClient extends Application {
	SpellingBeeGame spg;
	public void createNewGame() throws IOException{
			spg=new SpellingBeeGame();
	}
	
	public void start(Stage stage) throws IOException {
		createNewGame();
        Group root = new Group(); 
        
        Scene scene = new Scene(root, 650, 300); 
        scene.setFill(Color.BLACK);
        TabPane tp = new TabPane();
        ScoreTab score = new ScoreTab(spg);
        GameTab game = new GameTab(spg);
        
        tp.getTabs().add(game);
        tp.getTabs().add(score);
       
        root.getChildren().add(tp);
        
        
        
        stage.setTitle("Spelling Bee"); 
        stage.setScene(scene); 
        stage.show(); 
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}


