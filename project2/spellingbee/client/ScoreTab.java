/*
 * @Author William Trudel*/
package spellingbee.client;

import javafx.scene.Group;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class ScoreTab extends Tab{
	public ScoreTab(SpellingBeeGame s) {
		super("Score");
		Group root=new Group();
		GridPane g=new GridPane();
		g.setHgap(10);
		TextField cs=new TextField("Current Score");
		TextField gs=new TextField("Getting Started");
		TextField good=new TextField("Good");
		TextField amazing=new TextField("Amazing");
		TextField genius=new TextField("Genius");
		TextField qb=new TextField("Queen Bee");
		
		g.add(cs,0,0,1,1);
		g.add(gs,0,1,1,1);
		g.add(good,0,2,1,1);
		g.add(amazing,0,3,1,1);
		g.add(genius,0,4,1,1);
		g.add(qb,0,5,1,1);
		
		int[] rank=s.getBrackets();
		

		TextField csNum=new TextField(String.valueOf(s.getScore()));
		TextField gsNum=new TextField(String.valueOf(rank[0]));
		TextField goodNum=new TextField(String.valueOf(rank[1]));
		TextField amazingNum=new TextField(String.valueOf(rank[2]));
		TextField geniusNum=new TextField(String.valueOf(rank[3]));
		TextField qbNum=new TextField(String.valueOf(rank[4]));
		

		g.add(csNum,1,0,1,1);
		g.add(gsNum,1,1,1,1);
		g.add(goodNum,1,2,1,1);
		g.add(amazingNum,1,3,1,1);
		g.add(geniusNum,1,4,1,1);
		g.add(qbNum,1,5,1,1);
		
		root.getChildren().add(g);
		this.setContent(root);
	}
	
}
