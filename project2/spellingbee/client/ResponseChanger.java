package spellingbee.client;

import javafx.event.ActionEvent;

import javafx.event.EventHandler;
import javafx.scene.control.TextField;

/*
 * 
 * @Author Antonio Simonelli
 */

public class ResponseChanger implements EventHandler<ActionEvent>{
	private char letter;
	private TextField field;
	
	public ResponseChanger(char letter, TextField field) {
		this.letter = letter;
		this.field = field;
	}
	
	
	public void handle(ActionEvent e) {
		field.appendText(String.valueOf(letter));
		
	}

}
