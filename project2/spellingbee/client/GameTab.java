package spellingbee.client;

import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/*
 * 
 * @Author Antonio Simonelli
 */

public class GameTab extends Tab {
	

	public GameTab(SpellingBeeGame s) {
		super("Game");
		Group root = new Group();
		HBox buttons = new HBox();
		HBox text = new HBox();
		VBox vbox = new VBox();
		

		String letters=s.getAllLetters();
		

		Button letter1 = new Button(String.valueOf(letters.charAt(0)));
		letter1.setPrefWidth(50);
		Button letter2 = new Button(String.valueOf(letters.charAt(1)));
		letter2.setPrefWidth(50);
		Button letter3 = new Button(String.valueOf(letters.charAt(2)));
		letter3.setPrefWidth(50);
		Button letter4 = new Button(String.valueOf(letters.charAt(3)));
		letter4.setPrefWidth(50);
		Button letter5 = new Button(String.valueOf(letters.charAt(4)));
		letter5.setPrefWidth(50);
		Button letter6 = new Button(String.valueOf(letters.charAt(5)));
		letter6.setPrefWidth(50);
		Button letter7 = new Button(String.valueOf(letters.charAt(6)));
		letter7.setPrefWidth(50);
		TextField response = new TextField();
		response.setPrefWidth(50);
		Button submit = new Button("submit");

		submit.setPrefWidth(200);
		TextField valid = new TextField("make a word from the letters above");
		valid.setPrefWidth(200);
		TextField score = new TextField(String.valueOf(s.getScore()));
		score.setPrefWidth(200);
		
		SpellingBeeSubmit sbs=new SpellingBeeSubmit(response,valid,score,s);
		submit.setOnAction(sbs);
		
		ResponseChanger letterOne = new ResponseChanger(letters.charAt(0), response);
		letter1.setOnAction(letterOne);
		ResponseChanger letterTwo = new ResponseChanger(letters.charAt(1), response);
		letter2.setOnAction(letterTwo);
		ResponseChanger letterThree = new ResponseChanger(letters.charAt(2), response);
		letter3.setOnAction(letterThree);
		ResponseChanger letterFour = new ResponseChanger(letters.charAt(3), response);
		letter4.setOnAction(letterFour);
		ResponseChanger letterFive = new ResponseChanger(letters.charAt(4), response);
		letter5.setOnAction(letterFive);
		ResponseChanger letterSix = new ResponseChanger(letters.charAt(5), response);
		letter6.setOnAction(letterSix);
		ResponseChanger letterSeven = new ResponseChanger(letters.charAt(6), response);
		letter7.setOnAction(letterSeven);
		

		
		buttons.getChildren().addAll(letter1, letter2, letter3, letter4, letter5, letter6, letter7);
		text.getChildren().addAll(valid, score);
		vbox.getChildren().addAll(buttons, response, submit, text);
		root.getChildren().add(vbox);
		this.setContent(root);
		
	}
}
