/*
 * @Author William Trudel*/
package spellingbee.client;

import javafx.scene.control.TextField;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class SpellingBeeSubmit implements EventHandler<ActionEvent>{
	private TextField response;
	private TextField valid;
	private TextField score;
	private SpellingBeeGame sbg;
	
	public SpellingBeeSubmit(TextField response,TextField valid,TextField score,SpellingBeeGame sbg) {
		this.response=response;
		this.valid=valid;
		this.score=score;
		this.sbg=sbg;
	}
	
	@Override
	public void handle(ActionEvent e) {
		valid.setText(sbg.getMessage(response.getText()));
		if(sbg.getMessage(response.getText()).equals("good")) {
			sbg.getPointsForWord(response.getText());
		}
		score.setText(String.valueOf(sbg.getScore()));
	}
}
