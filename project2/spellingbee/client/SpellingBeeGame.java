/*
 * @Author William Trudel*/
package spellingbee.client;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

public class SpellingBeeGame implements ISpellingBeeGame{
	private char[] letters=new char[7];
	private char centerLetter;
	private int score;
	private HashSet<String> foundWords;
	private static HashSet<String> possibleWords;
	private int maxPoints;
	
	
	public static HashSet<String> createWordsFromFile(String path) throws IOException{
		Path p=Paths.get(path);
		List<String> file=Files.readAllLines(p);
		HashSet<String> dictionary=new HashSet<String>(file);
		return dictionary;
	}
	
	public SpellingBeeGame() throws IOException {
		SpellingBeeGame.possibleWords=createWordsFromFile("english.txt");
		Random r=new Random();
		for(int i=0;i<letters.length;i++)
			this.letters[i]=(char)('a'+r.nextInt(26));
		this.centerLetter=this.letters[r.nextInt(7)];
		this.foundWords=new HashSet<String>();
		this.maxPoints=0;
		for(String word:SpellingBeeGame.possibleWords) {
			if(checkValidity(word))
				maxPoints+=getPointsForWord(word);
		}
		this.score=0;
	}
	public SpellingBeeGame(String letters) throws IOException {
		SpellingBeeGame.possibleWords=createWordsFromFile("english.txt");
		Random r=new Random();
		for(int i=0;i<letters.length();i++)
			this.letters[i]=letters.charAt(i);
		this.centerLetter=this.letters[r.nextInt(7)];
		this.foundWords=new HashSet<String>();
		this.maxPoints=0;
		for(String word:possibleWords) {
			
			if(checkValidity(word))
				maxPoints+=getPointsForWord(word);
		}
		this.score=0;
	}
	
	public int[] getBrackets() {
		int[] rank= {(int)(maxPoints*0.25),(int)(maxPoints*0.5),(int)(maxPoints*0.75),(int)(maxPoints*0.9),(int)(maxPoints)};
		return rank;
	}
	
	public int getScore() {
		return this.score;
	}
	
	public char getCenterLetter() {
		return this.centerLetter;
	}
	
	public HashSet<String> getFoundWords(){
		return this.foundWords;
	}
	
	public String getAllLetters() {
		String allLetters="";
		for(int i=0;i<this.letters.length;i++)
			allLetters+=this.letters[i];
		return allLetters;
	}
	
	public String getMessage(String attempt) {
		if(checkValidity(attempt)&&SpellingBeeGame.possibleWords.contains(attempt)&&!foundWords.contains(attempt)){
			foundWords.add(attempt);
			return "good";
		}
		else
		return "that's no good";
	}
	
	public boolean checkValidity(String word) {
		
		boolean containsMidLetter=false;
		if(word.contains(String.valueOf(centerLetter)))
			containsMidLetter=true;
		
		boolean containsLetter=false;
		for(int i=0;i<word.length();i++) {
			for(int j=0;j<letters.length;j++) {
				if(word.charAt(i)==letters[j]) {
					containsLetter=true;
					break;
				}
			}
			if(!containsLetter)
				break;
		}
		if(containsMidLetter&&containsLetter)
			return true;
		else
			return false;
	}
	
	public int getPointsForWord(String attempt) {
		int score=0;
		if(attempt.length()==4)
			score=1;
		else if(attempt.length()>4)
			score=attempt.length();
		int letterCount=0;
		for(int i=0;i<letters.length;i++) {
			if(attempt.contains(String.valueOf(letters[i])))
				letterCount++;
		}
		if(letterCount==7)
			score+=7;
		this.score+=score;
		return score;
	}
}
