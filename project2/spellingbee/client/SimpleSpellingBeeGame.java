package spellingbee.client;



/*
 * 
 * @Author Antonio Simonelli
 */

public class SimpleSpellingBeeGame implements ISpellingBeeGame {
	private int points = 0;
	private char[] letters = {'u', 'o', 'p', 't', 'l', 'y', 'r'};
	private char center = letters[1];
	String attempt = "puzzler";
	String panagram = "poultry";
	String[] words = {"luck", "good", "puzzler"};
	

	public int[] getBrackets() {
		int[] rank = new int[5];
		rank[0] = 0;
		rank[1] = 5;
		rank[2] = 10;
		rank[3] = 15;
		rank[4] = 20;

		return rank;

	}

	public int getPointsForWord(String attempt) {
		if(attempt.length() < 4) {
			this.points += 0;
		}
		else if(attempt.length() == 4) {
			this.points += 1;
		}
		else if(attempt.equals(panagram)) {
			this.points += attempt.length() + 7;
		}
		else if(attempt.length() > 4) {
			this.points += attempt.length();
		}
		return this.points; 
	}
	
	public String getMessage(String attempt) {
		for(int i = 0; i<words.length; i++) {
			if(attempt.equals(words[i])) {
				return "good job";
			}
		}
		return "word does not exist";
	}
	
	public String getAllLetters() {
		String strLetters = "";
		for(int i = 0; i <letters.length; i++) {
			strLetters += String.valueOf(letters[i]);
		}
		return strLetters;
	}
	
	public char getCenterLetter() {
		return this.center;
	}
	
	public int getScore() {
		return this.points;
	}
}
